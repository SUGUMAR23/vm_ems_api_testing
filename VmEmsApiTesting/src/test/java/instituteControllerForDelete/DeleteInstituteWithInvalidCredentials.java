package instituteControllerForDelete;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.testng.annotations.Test;

public class DeleteInstituteWithInvalidCredentials {
	
	//With Bearer Token
	@Test(priority = 1)
	void deleteInvalidInstituteCodeWithBearerToken1()
	{
		String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDI0MDAxNCwiaWF0IjoxNjc5MDU2MDE0fQ.GRVqMIGt4a7DI86SaOQBI3l2K_Qqh45yE2CujtcIFCoUCpL8oshnUYQ-HDntfwvxhlfBZjuK1nt36FF6Z9KUcA";
		String institutecode = "wertyui87659809";	
		
		given()
			.headers("Authorization","Bearer "+token)
			.pathParam("mypath", institutecode)
			.queryParam("is_delete", "true")
		
		.when()
			.delete("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
			
		.then()
			.statusCode(400)
			.body("message", equalTo("Invalid institute code"))
			.log().all();
	}
	
	//With Bearer Token
	@Test(priority = 2)
	void deleteInstituteCodeAsSpaceWithBearerToken2()
	{
		String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDI0MDAxNCwiaWF0IjoxNjc5MDU2MDE0fQ.GRVqMIGt4a7DI86SaOQBI3l2K_Qqh45yE2CujtcIFCoUCpL8oshnUYQ-HDntfwvxhlfBZjuK1nt36FF6Z9KUcA";
		String institutecode = " ";	
		
		given()
			.headers("Authorization","Bearer "+token)
			.pathParam("mypath", institutecode)
			.queryParam("is_delete", "true")
		
		.when()
			.delete("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
			
		.then()
			.statusCode(400)
			.body("message", equalTo("Invalid institute code"))
			.log().all();
	}
	
	// Is delete = false
	@Test(priority = 3)
	void deletevalidInstituteCodeWithBearerToken3()
	{
		String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDI0MDAxNCwiaWF0IjoxNjc5MDU2MDE0fQ.GRVqMIGt4a7DI86SaOQBI3l2K_Qqh45yE2CujtcIFCoUCpL8oshnUYQ-HDntfwvxhlfBZjuK1nt36FF6Z9KUcA";
		String institutecode = "SDF678009917";	
		
		given()
			.headers("Authorization","Bearer "+token)
			.pathParam("mypath", institutecode)
			.queryParam("is_delete", "false")
		
		.when()
			.delete("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
			
		.then()
			.statusCode(400)
			.log().all();
	}
}
