package instituteControllerForDelete;

import static io.restassured.RestAssured.given;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.equalTo;

public class DeleteInstituteWithValidCredentials {

	String institutecode = "DFG204340112";
	
	//With Bearer Token
	@Test(priority = 1)
	void deleteInstituteWithBearerToken1()
	{
		String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDI0MDAxNCwiaWF0IjoxNjc5MDU2MDE0fQ.GRVqMIGt4a7DI86SaOQBI3l2K_Qqh45yE2CujtcIFCoUCpL8oshnUYQ-HDntfwvxhlfBZjuK1nt36FF6Z9KUcA";
		
		given()
			.headers("Authorization","Bearer "+token)
			.pathParam("mypath", institutecode)
			.queryParam("is_delete", "true")
	
		.when()
			.delete("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
		
		.then()
			.statusCode(200)
			.body("status", equalTo("success"))
			.body("message", equalTo("Institute deleted successfully"))
			.log().all();
	}
	
	
	String instituteCode1 = "XCV553439091";
	
	//Without Bearer Token
	@Test(priority = 2)
	void deleteInstituteWithoutBearerToken2()
	{
		
		given()
			.pathParam("mypath", instituteCode1)
			.queryParam("is_delete", "true")
	
		.when()
			.delete("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
		
		.then()
			.statusCode(401)
			.log().all();
	}
}
