package instituteControllerForStandardData;

import static io.restassured.RestAssured.given;
//import static org.hamcrest.Matchers.equalTo;
import org.testng.annotations.Test;

public class VeriftStandardDataWithInvalidInstituteCode {

	//Execute With Bearer Token
	@Test(priority = 1)
	void gettingStandardDataWithInvalidInstituteCode1()
	{
		String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDI0MDAxNCwiaWF0IjoxNjc5MDU2MDE0fQ.GRVqMIGt4a7DI86SaOQBI3l2K_Qqh45yE2CujtcIFCoUCpL8oshnUYQ-HDntfwvxhlfBZjuK1nt36FF6Z9KUcA";
		String code = "ABCDEFGH";
		given()
			.headers("Authorization","Bearer "+token)
			.pathParam("mypath", "standards-data")
			.queryParam("instituteCode", code)
	
		.when()
			.get("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
		
		.then()
			.statusCode(400)
			.log().all();
	}
	
	//Execute With Bearer Token
	@Test(priority = 2)
	void gettingStandardDataWithInvalidInstituteCode2()
	{
		String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDI0MDAxNCwiaWF0IjoxNjc5MDU2MDE0fQ.GRVqMIGt4a7DI86SaOQBI3l2K_Qqh45yE2CujtcIFCoUCpL8oshnUYQ-HDntfwvxhlfBZjuK1nt36FF6Z9KUcA";
		String code = "LUL601559897vbnm";
		given()
			.headers("Authorization","Bearer "+token)
			.pathParam("mypath", "standards-data")
			.queryParam("instituteCode", code)
	
		.when()
			.get("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
		
		.then()
			.statusCode(400)
			.log().all();
	}
	
	//Execute Without Bearer Token
	@Test(priority = 3)
	void gettingStandardDataWithInvalidInstituteCode3()
	{
		String token = "UzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDI0MDAxNCwiaWF0IjoxNjc5MDU2MDE0fQ.GRVqMIGt4a7DI86SaOQBI3l2K_Qqh45yE2CujtcIFCoUCpL8oshnUYQ-HDntfwvxhlfBZjuK1nt36FF6Z9KUcA";
		String code = "LUL601559897vbnm";
		given()
			.headers("Authorization","Bearer "+token)
			.pathParam("mypath", "standards-data")
			.queryParam("instituteCode", code)
	
		.when()
			.get("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
		.then()
			.statusCode(500)
			.log().all();
	}
	
	//Execute With Bearer Token
	@Test(priority = 4)
	void gettingStandardDataWithInvalidInstituteCode4()
	{
		String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDI0MDAxNCwiaWF0IjoxNjc5MDU2MDE0fQ.GRVqMIGt4a7DI86SaOQBI3l2K_Qqh45yE2CujtcIFCoUCpL8oshnUYQ-HDntfwvxhlfBZjuK1nt36FF6Z9KUcA";
		String code = "123456789";
		given()
			.headers("Authorization","Bearer "+token)
			.pathParam("mypath", "standards-data")
			.queryParam("instituteCode", code)
	
		.when()
			.get("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
		
		.then()
			.statusCode(400)
			.log().all();
	}
	
	//Execute With Bearer Token
	@Test(priority = 5)
	void gettingStandardDataWithInvalidInstituteCode5()
	{
		String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDI0MDAxNCwiaWF0IjoxNjc5MDU2MDE0fQ.GRVqMIGt4a7DI86SaOQBI3l2K_Qqh45yE2CujtcIFCoUCpL8oshnUYQ-HDntfwvxhlfBZjuK1nt36FF6Z9KUcA";
		String code = " ";
		given()
			.headers("Authorization","Bearer "+token)
			.pathParam("mypath", "standards-data")
			.queryParam("instituteCode", code)
	
		.when()
			.get("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
		
		.then()
			.statusCode(400)
			.log().all();
	}
}
