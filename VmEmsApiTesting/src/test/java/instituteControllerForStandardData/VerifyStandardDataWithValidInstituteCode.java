package instituteControllerForStandardData;

import static io.restassured.RestAssured.given;
import org.testng.annotations.Test;

public class VerifyStandardDataWithValidInstituteCode {

	//Execute Without Bearer Token
	@Test(priority = 1)
	void gettingStandardDataWithoutBearerToken()
	{
		
		given()
			.contentType("application/json")
			.queryParam("instituteCode ", "LUL601559897")
		
		.when()
			.get("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/standards-data")
		
		.then()
			.statusCode(401)
			.log().all();
	}
	
	//Execute With Bearer Token
	@Test(priority = 2)
	void gettingStandardDataWithBearerToken()
	{
		String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDI0MDAxNCwiaWF0IjoxNjc5MDU2MDE0fQ.GRVqMIGt4a7DI86SaOQBI3l2K_Qqh45yE2CujtcIFCoUCpL8oshnUYQ-HDntfwvxhlfBZjuK1nt36FF6Z9KUcA";
		String code = "LUL601559897";
		given()
			.headers("Authorization","Bearer "+token)
			.pathParam("mypath", "standards-data")
			.queryParam("instituteCode", code)
	
		.when()
			.get("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
		
		.then()
			.statusCode(200)
			.log().all();
	}
}
