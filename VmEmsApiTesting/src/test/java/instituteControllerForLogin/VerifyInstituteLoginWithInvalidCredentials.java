package instituteControllerForLogin;

import static io.restassured.RestAssured.given;
import java.util.HashMap;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.equalTo;

public class VerifyInstituteLoginWithInvalidCredentials {

	//Enter Invalid Institute Code
	@Test(priority = 1)
	void VerifyInstituteRegisterWithInValidInstituteCode1()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL60155989723456");
		insertData.put("email", "sugu2351998+1@gmail.com");
		insertData.put("password", "password");
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/login")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid credentials"))
			.log().all();
	}
	
	
	//Enter Invalid Mail ID
	@Test(priority = 2)
	void VerifyInstituteRegisterWithInValidMailID2()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL601559897");
		insertData.put("email", "sugu2351998+1111@gmail.com");
		insertData.put("password", "password");
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/login")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid credentials"))
			.log().all();
	}
	
	//Enter Invalid Mail ID
	@Test(priority = 3)
	void VerifyInstituteRegisterWithInValidPassword3()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL601559897");
		insertData.put("email", "sugu2351998+1@gmail.com");
		insertData.put("password", "passwordxyz");
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/login")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid credentials"))
			.log().all();
	}
	
	//Enter Invalid Credentials (Only Space)
	@Test(priority = 4)
	void VerifyInstituteRegisterWithInValidCredentials4()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", " ");
		insertData.put("email", " ");
		insertData.put("password", " ");
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/login")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid credentials"))
			.log().all();
	}
	
	//Enter Invalid Credentials (Null)
	@Test(priority = 5)
	void VerifyInstituteRegisterWithInValidCredentialsNull5()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "");
		insertData.put("email", "");
		insertData.put("password", "");
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/login")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid credentials"))
			.log().all();
	}
	
	
	//Enter Invalid Credentials (With Bearer Token)
	@Test(priority = 6)
	void VerifyInstituteRegisterWithInValidCredentialsWithBearerToken6()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "DFGHJK3456789");
		insertData.put("email", "345678tfgujk");
		insertData.put("password", "fghjk56789");
		
		String BearerToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDE3NDA5NCwiaWF0IjoxNjc4OTkwMDk0fQ.Z44sqPf7ALmY6U7SoVsRnRsxEY2uUHPq2FKQ0MrrCtKfICy6dHZ_1pfoEHx714iw-qymW7N7wwwrjr42fYkg6g";
		
		given()
			.header("Authorization", "Bearer "+BearerToken)
			.pathParam("mypath", "login")
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid credentials"))
			.log().all();
	}
}
