package instituteControllerForLogin;

import static io.restassured.RestAssured.given;
import java.util.HashMap;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.equalTo;

public class VerifyInstituteLoginWithValidCredentials {

	@Test(priority = 1)
	void VerifyInstituteRegisterWithValidCrendentials1()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL601559897");
		insertData.put("email", "sugu2351998+1@gmail.com");
		insertData.put("password", "password");
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/login")
		
		.then()
			.statusCode(200)
			.body("status", equalTo("success"))
			.log().all();
	}
	
	
	@Test(priority = 2)
	void VerifyInstituteRegisterWithValidCrendentialsWithBearerToken()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL601559897");
		insertData.put("email", "sugu2351998+1@gmail.com");
		insertData.put("password", "password");
		
		String BearerToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDE3NDA5NCwiaWF0IjoxNjc4OTkwMDk0fQ.Z44sqPf7ALmY6U7SoVsRnRsxEY2uUHPq2FKQ0MrrCtKfICy6dHZ_1pfoEHx714iw-qymW7N7wwwrjr42fYkg6g";
		
		given()
			.header("Authorization", "Bearer "+BearerToken)
			.pathParam("mypath", "login")
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
		
		.then()
			.statusCode(200)
			.body("status", equalTo("success"))
			.log().all();
	}
}
