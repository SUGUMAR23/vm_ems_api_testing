package instituteControllerForVerifyOTP;

import static io.restassured.RestAssured.given;
import java.util.HashMap;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.equalTo;

public class VerifyOTPWithInvalidCredentials {

	String otp = "722961";
	
	//Enter Invalid Institute Code
	@Test(priority = 1)
	void VerifyOTPWithInvalidInstituteCode1()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL60155989723456");
		insertData.put("email", "sugu2351998+1@gmail.com");
		insertData.put("password", "password");
		insertData.put("otp", otp);
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/verify-otp")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid otp"))
			.log().all();
	}
	
	//Enter Invalid Mail ID
	@Test(priority = 2)
	void VerifyOTPWithInvalidMailID2()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL601559897");
		insertData.put("email", "sugu2351998+991@gmail.com");
		insertData.put("password", "password");
		insertData.put("otp", otp);
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/verify-otp")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid otp"))
			.log().all();
	}
	
	//Enter Invalid Password
	@Test(priority = 3)
	void VerifyOTPWithInvalidPassword3()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL601559897");
		insertData.put("email", "sugu2351998+1@gmail.com");
		insertData.put("password", "passwordxyz");
		insertData.put("otp", otp);
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/verify-otp")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid otp"))
			.log().all();
	}
	
	
	//Enter Invalid OTP
	@Test(priority = 4)
	void VerifyOTPWithInvalidOTP4()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL601559897");
		insertData.put("email", "sugu2351998+1@gmail.com");
		insertData.put("password", "passwordxyz");
		insertData.put("otp", "765490");
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/verify-otp")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid otp"))
			.log().all();
	}
	
	//Execute Without OTP
	@Test(priority = 5)
	void ExecuteWithoutOTP5()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL601559897");
		insertData.put("email", "sugu2351998+1@gmail.com");
		insertData.put("password", "password");
		//insertData.put("otp", otp1);
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/verify-otp")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid otp"))
			.log().all();
	}
	
	//Execute Without Institute Code
	@Test(priority = 6)
	void ExecuteWithoutInstituteCode6()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "");
		insertData.put("email", "sugu2351998+1@gmail.com");
		insertData.put("password", "password");
		insertData.put("otp", otp);
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/verify-otp")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid otp"))
			.log().all();
	}
	
	//Execute Without Institute Code
	@Test(priority = 7)
	void ExecuteWithoutMailID7()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL601559897");
		insertData.put("email", "");
		insertData.put("password", "password");
		insertData.put("otp", otp);
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/verify-otp")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid otp"))
			.log().all();
	}
	
	//Execute Without Password
	@Test(priority = 8)
	void ExecuteWithoutPassword8()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL601559897");
		insertData.put("email", "sugu2351998+1@gmail.com");
		insertData.put("password", "");
		insertData.put("otp", otp);
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/verify-otp")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid otp"))
			.log().all();
	}
	
	//Execute With Bearer Token Without OTP
	@Test(priority = 9)
	void ExecuteWithoutPassword9()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL601559897");
		insertData.put("email", "sugu2351998+1@gmail.com");
		insertData.put("password", "password");
		insertData.put("otp", "");
		
		String BearerToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDE3NDA5NCwiaWF0IjoxNjc4OTkwMDk0fQ.Z44sqPf7ALmY6U7SoVsRnRsxEY2uUHPq2FKQ0MrrCtKfICy6dHZ_1pfoEHx714iw-qymW7N7wwwrjr42fYkg6g";
		
		given()
		.header("Authorization", "Bearer "+BearerToken)
		.pathParam("mypath", "verify-otp")
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")
		
		.then()
			.statusCode(200)
			.body("message", equalTo("Invalid otp"))
			.log().all();
	}
}
