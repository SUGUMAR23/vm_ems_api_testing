package instituteControllerForVerifyOTP;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.util.HashMap;

import org.testng.annotations.Test;

public class OTPexpiresVerification {

	String otp = "722961";

	// Verify OTP With Valid Credentials but Without Bearer Token
	@Test(priority = 1)
	void VerifyOTPWithCredentials() {
		HashMap<String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL601559897");
		insertData.put("email", "sugu2351998+1@gmail.com");
		insertData.put("password", "password");
		insertData.put("otp", otp);

		given()
			.contentType("application/json")
			.body(insertData)

		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/verify-otp")

		.then()
			.statusCode(200)
			.body("message", equalTo("Otp expired"))
			.log().all();
	}

	// Verify OTP With Valid Credentials but Without Bearer Token
	@Test(priority = 2)
	void VerifyOTPWithCredentialsWithBearerToken() {
		HashMap<String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteCode", "LUL601559897");
		insertData.put("email", "sugu2351998+1@gmail.com");
		insertData.put("password", "password");
		insertData.put("otp", otp);

		String BearerToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdWd1MjM1MTk5OCsxQGdtYWlsLmNvbSIsImV4cCI6MTY4NDE3NDA5NCwiaWF0IjoxNjc4OTkwMDk0fQ.Z44sqPf7ALmY6U7SoVsRnRsxEY2uUHPq2FKQ0MrrCtKfICy6dHZ_1pfoEHx714iw-qymW7N7wwwrjr42fYkg6g";

		given()
			.header("Authorization", "Bearer " + BearerToken)
			.pathParam("mypath", "verify-otp")
			.contentType("application/json").body(insertData)

		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/{mypath}")

		.then()
			.statusCode(200)
			.body("message", equalTo("Otp expired"))
			.log().all();
	}
}
