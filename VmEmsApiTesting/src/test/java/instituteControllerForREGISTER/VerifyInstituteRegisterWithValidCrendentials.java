package instituteControllerForREGISTER;

import java.util.HashMap;
import static io.restassured.RestAssured.*;
import org.testng.annotations.Test;

public class VerifyInstituteRegisterWithValidCrendentials {

	@Test
	void VerifyInstituteRegisterWithValidCrendentials1()
	{
		HashMap <String, String> insertData = new HashMap<String, String>();
		insertData.put("instituteName", "SASTRA Deemed University");
		insertData.put("adminName", "R.Sethuraman");
		insertData.put("place", "Thanjavur");
		insertData.put("isDeleted", "false");
		insertData.put("instituteEmail", "sugu2351998+84@gmail.com");
		insertData.put("institutePassword", "Sastra@84");
		insertData.put("pinCode", "670068");
		insertData.put("noOfUsers", "100000");
		insertData.put("noOfExams", "30");
		insertData.put("noOfMonths", "3");
		insertData.put("profilePic", "3");
		
		given()
			.contentType("application/json")
			.body(insertData)
		
		.when()
			.post("http://emscorejava-env.eba-ggrpuqsi.ap-south-1.elasticbeanstalk.com/institute/register")
		
		.then()
			.statusCode(201)   
			.log().all();
	}
}
